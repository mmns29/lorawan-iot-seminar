// Decode decodes an array of bytes into an object.
//  - fPort contains the LoRaWAN fPort number
//  - bytes is an array of bytes, e.g. [225, 230, 255, 0]
// The function must return an object, e.g. {"temperature": 22.5}
function Decode(fPort, bytes) {
  var b0 = bytes[0] << 8 ;
  var b1 = bytes[1] ;
  var temperature = b0 + b1  ;
  temperature = (temperature / 10) -30 ;
  
  var b2 = bytes[2] << 8 ;
  var b3 = bytes[3] ;
  var pressure = b2 + b3 ;
  pressure = pressure / 10
  
  var b4 = bytes[4] << 8 ;
  var b5 = bytes[5] ;
  var humidity = b4 + b5;
  humidity = humidity / 10
  
  
  var message = {
    temperature : temperature,
    pressure : pressure,
    humidity : humidity
  }
  return message;
}