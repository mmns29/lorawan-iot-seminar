"""
Example for using the RFM9x Radio with Raspberry Pi and LoRaWAN

Learn Guide: https://learn.adafruit.com/lora-and-lorawan-for-raspberry-pi
Author: Brent Rubell for Adafruit Industries
"""

from sense_emu  import  SenseHat 
import threading
import time
import subprocess
import busio
from digitalio import DigitalInOut, Direction, Pull
import board
# Import thte SSD1306 module.
import adafruit_ssd1306
# Import Adafruit TinyLoRa
from adafruit_tinylora.adafruit_tinylora import TTN, TinyLoRa

# Button A
btnA = DigitalInOut(board.D5)
btnA.direction = Direction.INPUT
btnA.pull = Pull.UP

# Button B
btnB = DigitalInOut(board.D6)
btnB.direction = Direction.INPUT
btnB.pull = Pull.UP

# Button C
btnC = DigitalInOut(board.D12)
btnC.direction = Direction.INPUT
btnC.pull = Pull.UP

# Create the I2C interface.
i2c = busio.I2C(board.SCL, board.SDA)

# 128x32 OLED Display
display = adafruit_ssd1306.SSD1306_I2C(128, 32, i2c, addr=0x3c)
# Clear the display.
display.fill(0)
display.show()
width = display.width
height = display.height

# TinyLoRa Configuration
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
cs = DigitalInOut(board.CE1)
irq = DigitalInOut(board.D22)
rst = DigitalInOut(board.D25)

# Device Address, 4 Bytes, MSB
devaddr = bytearray([0x00, 0x9e, 0x4e, 0x6b])

#Network Key, 16 Bytes, MSB
nwkey = bytearray([0x29, 0xea, 0xba, 0x7e, 0x07, 0x8c, 0xc9, 0x60, 0x01, 0x77, 0x69, 0xf0, 0x33, 0x5d, 0x29, 0xe4])

#Application Key, 16 Bytess, MSB
appkey = bytearray([0x87, 0x6d, 0x98, 0xcf, 0xe8, 0x4a, 0xf8, 0xa7, 0x13, 0x6d, 0x8f, 0x08, 0x1d, 0x1e, 0x09, 0x97])

# Initialize Network configuration
my_config = TTN(devaddr, nwkey, appkey, country='EU')
# Initialize lora object
lora = TinyLoRa(spi, cs, irq, rst, my_config)
# 2b array to store sensor data
data_pkt = bytearray(6)
sensors = SenseHat()
# time to delay periodic packet sends (in seconds)
data_pkt_delay = 5.0


def send_pi_data_periodic():
    threading.Timer(data_pkt_delay, send_pi_data_periodic).start()
    print("Sending periodic data...")
    send_pi_data(data_pkt)
    print('LoRaWAN message payload:', data_pkt)

def send_pi_data(data_pkt):
    # Send data packet
    lora.send_data(data_pkt, len(data_pkt), lora.frame_counter)
    lora.frame_counter += 1
    display.fill(0)
    display.text('Sent Data to:', 10, 0, 1)
    display.text('iot.ei.thm.de' , 10, 15, 1)
    print('Data sent!')
    display.show()
    time.sleep(0.5)

while True:
    packet = None
    # draw a box to clear the image
    display.fill(0)
    display.text('RasPi LoRaWAN', 10, 0, 1)
    display.text('IoT LAB' , 10, 15, 1)
        
    # read sendHat values 
    temperature = int (sensors.temperature + 30) * 10 
    data_pkt[0] = temperature >> 8 
    data_pkt[1] = temperature & 0xff
    pressure = int (sensors.pressure * 10)
    data_pkt[2] = pressure >> 8
    data_pkt [3] = pressure & 0xff
    humidity = int (sensors.humidity * 10 )
    data_pkt[4] = humidity >> 8
    data_pkt[5] = humidity & 0xff
        

    if not btnA.value:
        # Send Packet
        send_pi_data(data_pkt)
    if not btnB.value:
        # Display values
        display.fill(0)
        display.text('Values', 45, 0, 1)
        display.text(str(temperature / 10 - 30)+ ',' + str (pressure / 10) + ',' + str(humidity / 10), 25, 15, 1)
        display.show()
        time.sleep(0.1)
    if not btnC.value:
        display.fill(0)
        display.text('* Periodic Mode *', 15, 0, 1)
        display.show()
        time.sleep(0.5)
        send_pi_data_periodic()


    display.show()
    time.sleep(.1)
